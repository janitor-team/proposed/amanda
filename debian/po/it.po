# Italian (it) translation of debconf templates for amanda
# Copyright (C) 2004 Software in the Public Interest
# This file is distributed under the same license as the amanda package.
# Luca Monducci <luca.mo@tiscali.it>, 2004-2006.
#
msgid ""
msgstr ""
"Project-Id-Version: amanda 1:2.5.0p2 italian debconf templates\n"
"Report-Msgid-Bugs-To: amanda@packages.debian.org\n"
"POT-Creation-Date: 2008-04-16 01:13-0600\n"
"PO-Revision-Date: 2006-05-30 10:30+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: error
#. Description
#: ../templates:1001
msgid "Please merge /var/lib/amandates and /var/lib/amanda/amandates"
msgstr "Unire /var/lib/amandates e /var/lib/amanda/amndates."

#. Type: error
#. Description
#: ../templates:1001
msgid ""
"You have both /var/lib/amandates and /var/lib/amanda/amandates. Please "
"review the files, and merge the contents you care about to the /var/lib/"
"amanda/amandates location, and remove the old file /var/lib/amandates."
msgstr ""
"Esistono sia /var/lib/amandates che /var/lib/amanda/amandates. Controllare i "
"file e unire tutte le parti utili in /var/lib/amanda/amandates e poi "
"rimuovere il file /var/lib/amanadates."
