# translation of ar.po to
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans#
#    Developers do not need to manually edit POT or PO files.
#
# Ossama M. Khayat, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: amanda_1:2.4.5-1\n"
"Report-Msgid-Bugs-To: amanda@packages.debian.org\n"
"POT-Creation-Date: 2008-04-16 01:13-0600\n"
"PO-Revision-Date: 2010-09-09 04:51+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#. Type: error
#. Description
#: ../templates:1001
msgid "Please merge /var/lib/amandates and /var/lib/amanda/amandates"
msgstr "الرجاء دمج /var/lib/amandates و /var/lib/amanda/amandates"

#. Type: error
#. Description
#: ../templates:1001
msgid ""
"You have both /var/lib/amandates and /var/lib/amanda/amandates. Please "
"review the files, and merge the contents you care about to the /var/lib/"
"amanda/amandates location, and remove the old file /var/lib/amandates."
msgstr ""
"لديك كلاً من /var/lib/amandates و /var/lib/amanda/amandates. الرجاء معاينة "
"الملفات، ودمج المحتويات التي تهمك حول موقع /var/lib/amanda/amandates، وإزالة "
"الملف /var/lib/amandates القديم."

