# Portuguese translation of amanda debconf messages.
# Copyright (C) 2007 Carlos Lisboa
# This file is distributed under the same license as the amanda package.
# Carlos Lisboa <carloslisboa@gmail.com>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: amanda\n"
"Report-Msgid-Bugs-To: amanda@packages.debian.org\n"
"POT-Creation-Date: 2008-04-16 01:13-0600\n"
"PO-Revision-Date: 2007-02-04 12:24+0000\n"
"Last-Translator: Carlos Lisboa <carloslisboa@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: error
#. Description
#: ../templates:1001
msgid "Please merge /var/lib/amandates and /var/lib/amanda/amandates"
msgstr ""
"Por favor faça a fusão de /var/lib/amandates e /var/lib/amanda/amandates"

#. Type: error
#. Description
#: ../templates:1001
msgid ""
"You have both /var/lib/amandates and /var/lib/amanda/amandates. Please "
"review the files, and merge the contents you care about to the /var/lib/"
"amanda/amandates location, and remove the old file /var/lib/amandates."
msgstr ""
"Possui tanto o /var/lib/amandates como o /var/lib/amanda/amandates. Por "
"favor reveja esses ficheiros, e faça a fusão do conteúdo que pretende para o "
"local /var/lib/amanda/amandates, e remova o ficheiro antigo /var/lib/"
"amandates."
